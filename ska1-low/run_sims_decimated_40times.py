import os
from astropy.time import Time, TimeDelta
from astropy.io import fits
import matplotlib.pyplot as plt
import oskar
import numpy

def get_start_time(ra0_deg, length_sec):
    """Returns optimal start time for field RA and observation length."""
    t = Time("2000-01-01 00:00:00", scale="utc", location=("116.764d", "0d"))
    dt_hours = (24.0 - t.sidereal_time("apparent").hour) / 1.0027379
    dt_hours += (ra0_deg / 15.0)
    start = t + TimeDelta(dt_hours * 3600.0 - length_sec / 2.0, format="sec")
    return start.value


def plot_source_fluxes(sky_model, title, filename=None):
    """Plots source flux histogram."""
    data = sky_model.to_array()
    min_flux = numpy.min(data[:, 2])
    max_flux = numpy.max(data[:, 2])
    bins = numpy.logspace(numpy.log10(min_flux), numpy.log10(max_flux), 50)
    plt.hist(data[:, 2], bins=bins)
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.xlabel('Flux [Jy]')
    plt.ylabel('source count')
    plt.title(title)
    if filename:
        plt.savefig(filename)
    else:
        plt.show()
    plt.close()


def main():
    """Main function."""
    # Open GLEAM catalogue FITS file.
    sky_dir = "./"
    gleam_fits = fits.getdata(sky_dir + "GLEAM_EGC.fits", 1)

    # Get data from named columns.
    ra_deg = gleam_fits["RAJ2000"]
    dec_deg = gleam_fits["DEJ2000"]
    flux_jy = gleam_fits["int_flux_151"]
    zeros = numpy.zeros_like(flux_jy)
    ref_freq_hz = 151e6 * numpy.ones_like(flux_jy)
    alpha = gleam_fits["alpha"]
    alpha[numpy.isnan(alpha)] = 0

    # Create a sky model.
    gleam_sky_array = numpy.column_stack((
        ra_deg, dec_deg, flux_jy, zeros, zeros, zeros, ref_freq_hz, alpha))

    # Remove negative source flux values(!).
    neg_indices = numpy.where(gleam_sky_array[:, 2] < 0)
    gleam_sky_array = numpy.delete(gleam_sky_array, neg_indices, axis=0)
    gleam_sky = oskar.Sky.from_array(gleam_sky_array)

    # Filter the sky model.
    ra0_deg = 0.0
    dec0_deg = -27.0
    gleam_sky.filter_by_radius(0, 10, ra0_deg, dec0_deg)
    plot_source_fluxes(
        gleam_sky,
        "GLEAM only (original; %d sources)" % gleam_sky.num_sources,
        "source_fluxes_GLEAM_original.png"
    )

    gleam_sky.filter_by_flux(0.1, 1e9)
    plot_source_fluxes(
        gleam_sky,
        "GLEAM only (above 0.1 Jy; %d sources)" % gleam_sky.num_sources,
        "source_fluxes_GLEAM_filtered.png"
    )

    # Create a fainter source population.
    faint_sky = oskar.Sky.generate_random_power_law(8000000, 1e-4, 0.1, -1)
    faint_sky.filter_by_radius(0, 5, ra0_deg, dec0_deg)

    # Create some random spectral index values for the faint sources.
    num_faint_sources = faint_sky.num_sources
    faint_sky_array = faint_sky.to_array()
    faint_sky_array[:, 6] = ref_freq_hz[0]
    faint_sky_array[:, 7] = numpy.random.uniform(
        low=-1.5, high=0, size=num_faint_sources)
    faint_sky = oskar.Sky.from_array(faint_sky_array)
    plot_source_fluxes(
        faint_sky,
        "Faint sources only (%d sources)" % faint_sky.num_sources,
        "source_fluxes_faint.png"
    )
    gleam_sky.append(faint_sky)
    plot_source_fluxes(
        gleam_sky,
        "Combined sky models (%d sources)" % gleam_sky.num_sources,
        "source_fluxes_combined.png"
    )

    # Save to file.
    gleam_sky.save("GLEAM_filtered.txt")
    #return

    # Simulation parameters.
    tel_dir = "./"
    tel_model = tel_dir + "SKA1-LOW_SKO-0000422_Rev3_38m.tm"
    length_sec = 14400
    base_params = {
        "simulator/max_sources_per_chunk": 64*1024,
        "simulator/write_status_to_log_file": True,
        "simulator/keep_log_file": True,
        "simulator/double_precision":True,
        "observation/phase_centre_ra_deg": ra0_deg,
        "observation/phase_centre_dec_deg": dec0_deg,
        "observation/start_frequency_hz": 140e6,
        "observation/num_channels": 100,
        "observation/frequency_inc_hz": 200e3,
        "observation/start_time_utc": get_start_time(ra0_deg, length_sec),
        "observation/length": length_sec,
        "observation/num_time_steps": 40,
        "telescope/input_directory": tel_model,
        "telescope/gaussian_beam/fwhm_deg": 5.5,  # 1.22 lambda/d
        "telescope/gaussian_beam/ref_freq_hz": 100e6,
        # 'telescope/ionosphere_screen_type':'External',
        # 'telescope/external_tec_screen/input_fits_file':'test_screen_60s.fits',
        # 'telescope/external_tec_screen/screen_pixel_size_m':100.0,
        "interferometer/channel_bandwidth_hz": 10e3,
        "interferometer/time_average_sec": 0.9,
    }

    # Parameter sets.
    param_sets = {
        "scalar_realistic_dipole_element_beams_noscreen": {
            "telescope/station_type": "Aperture array",
            "telescope/pol_mode": "Scalar",
            "telescope/aperture_array/element_pattern/functional_type": "Dipole",
            "interferometer/force_polarised_ms": True,
        },
        "realistic_dipole_element_beams_noscreen": {
            "telescope/station_type": "Aperture array",
            "telescope/pol_mode": "Full",
            "telescope/aperture_array/element_pattern/functional_type": "Dipole",
        },
    }

    # Loop over parameter sets.
    for name, param_set in param_sets.items():

        # Get MS name. Skip if already done.
        ms_name = "outputms/"+name + ".ms"
        if os.path.exists(ms_name):
            continue

        # Run simulation.
        settings_sim = oskar.SettingsTree("oskar_sim_interferometer")
        settings_sim.from_dict(base_params)
        settings_sim.from_dict(param_set)
        settings_sim["interferometer/ms_filename"] = ms_name
        sim = oskar.Interferometer(settings=settings_sim)
        sim.set_sky_model(gleam_sky)
        sim.run()

        # # Imaging parameters.
        # params_img = {
        #     "image/fov_deg": 5.5,
        #     "image/size": 8192,
        #     "image/algorithm": "W-projection",
        #     "image/fft/use_gpu": True,
        #     "image/fft/grid_on_gpu": True,
        #     "image/input_vis_data": ms_name,
        #     "image/root_path": "full_array_without_screen/"+name + "_image"
        # }

        # # Make image.
        # settings_img = oskar.SettingsTree("oskar_imager")
        # settings_img.from_dict(params_img)
        # imager = oskar.Imager(settings=settings_img)
        # imager.run()

if __name__ == "__main__":
    main()
